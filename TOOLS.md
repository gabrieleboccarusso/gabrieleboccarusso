# Tools

this file describes my personal coding stack and how to set it up too.

## Working Stack

- Operative System: Debian 12
- Frontend:
    - Language: Typescript
    - Framework: Angular
    - Styles: Bootstrap
- Backend:
    - Language: Java
    - Framework: Spring Boot
- Database: todo
- DevSecOps: todo
- Mobile: Angular PWA
- Testing:
    - API: postman

## Set up

### The base of it all: Debian OS

The latest *stable* release of debian is currently debian 12 available for download [here](https://www.debian.org/). When clicking on download it should automatically identify the architecture of the visiting computer otherwise is possible to download the desired iso.

#### installation process

- setting up user
- setting up UI (gnome)

#### Fixing problems

- Fix single touch in touchpad not working [easy]
- Fix right click not working [easy]

### Software

- Firefox
    - already installed but in ESR
- Postman
    - debian installation requires cli steps - [link](https://gist.github.com/Akhil-Suresh/e036a52bd00104ab21e9891224157809)
- Codium
    - gitlab workflow
    - extension pack for java
    - Spring boot extension pack
    - monokai pro

### Websites

- google drive
- draw.io
- gitlab
- gmail
- linkedin
